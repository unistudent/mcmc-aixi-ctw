import queue
import shelve

import sys
from random import random, getrandbits
from math import log

import math

from multiprocessing import Process

threshhold = 31 #maximum depth of trees supported on my laptop
discount_value = 0.95 #discount value for the log probability
class CTNode(object):
    """
    Instead of being represented by objects with pointers
    Tress could be represented by a list, which saves time
    """
    def __init__(self, depth, index):
        self.log_prob_estimated = math.log2(0.5)  # log KT estimated probability
        self.log_prob_weighted = math.log2(0.5)  # double - log weighted blocked probability
        self.count_zero = 0  # int number of times context 0 be encountered
        self.count_one = 0  # int number of times context 1 be encountered
        if type(index)==int:
            self.index = int(index)
            self.child_zero = (index+1)*2-1  # 0 child
            self.child_one = (index+1)*2  # 1 child
        elif type(index)==str:
            self.index = str(index)
            self.child_zero = str((int(index)+1)*2-1)  # 0 child
            self.child_one = str((int(index)+1)*2)  # 1 child

        self.depth = depth
        #print "Node at level ", self.depth, " is created."



    def count(self):
        """

        :return: int - the number of times this context has been visited
        """
        return self.count_zero + self.count_one




    def logKTMul(self, sym):
        """
        compute the logarithm of the KT-estimator update multiplier
        Formula 20 in Vanness paper
        :param sym: 0 or 1
        :return:
        """
        if sym=="0":
            #if i substitue 0.5 to 1, "domain error" will be generated
            return math.log2((self.count_zero +0.5)/(self.count()+1))
        elif sym=="1":
            return math.log2((self.count_one +0.5)/(self.count()+1))



class ContextTree(object):
    def __init__(self, depth):
        """
        create a context tree of specified maximum depth
        :param depth: int total tree depth
        :return:
        """
        self.depth = depth #total tree depth
        if depth<=threshhold:
            self.node_list = [None]*(2**(self.depth+1)-1) #Maximum number of nodes in a binary tree is 2^depth -1
        else:
            self.node_list = shelve.open("tempfile")
        self.clear()

    def clear(self):
        """
        # keep the depth unchanged
        :return:
        """
        if self.depth<=threshhold:
            self.node_list[0] = CTNode(0, 0) #Root Node initialisation
        else:
            self.node_list["0"]=CTNode(0, 0) #Root Node initialisation
        self.history = list()  # a list of symbols
        self.printCTW()

    def resetProbability(self):
        """
        for n in self.node_list:
            if type(n)==CTNode:
                n.log_prob_estimated= math.log2((n.count_one +0.5)/(n.count()+1))
        """

    def printCTW(self):
        """
        Print the context tree in a visual way for diagnosis
        :return:
        """
        #TODO
        return

    def shouldCreateNewNode(self, i):
        """

        :param i: int or string, index of a node
        :return: Bool
        """
        if self.depth<=threshhold:
            return self.node_list[i]==None
        else: #case of using shelve
            return str(i) not in self.node_list


    def updateSymbol(self, sym):
        """
        Updates the context tree with a new binary symbol
        :param sym:
        :return: void
        """

        #if history is short, just add the symbols
        if len(self.history)<self.depth:
            self.history.append(sym)
            return



        #look back the past "self.depth" symbols
        depth_counter = self.depth #minus 1 in every iteration

        while depth_counter>=0:
            #locate the node
            relevant_node_index = self.__findNode(depth_counter)
            #update the node from the deep end to the root
            if self.depth>threshhold:
                relevant_node_index = str(relevant_node_index) #cater to the shelve API

            if self.shouldCreateNewNode(relevant_node_index):
                try:
                    self.node_list[relevant_node_index]=CTNode(depth_counter, relevant_node_index)
                except :
                    print (relevant_node_index)
                    sys.exit(0)

            """
            Update the node after having observed a new symbol.
            :param sym: new symbol
            :return: void
            """
            self.node_list[relevant_node_index].log_prob_estimated=self.node_list[relevant_node_index].log_prob_estimated*discount_value
            self.node_list[relevant_node_index].log_prob_estimated += self.node_list[relevant_node_index].logKTMul(sym)
            if sym=="0":
                self.node_list[relevant_node_index].count_zero+=1
            else:
                self.node_list[relevant_node_index].count_one+=1
            self.updateLogProbability(relevant_node_index)

            depth_counter -=1

        self.history +=  list(sym) #list() is needed because history is a list
        #updating sym in history in the delegated method to maximize autonomy of this method
        return

    def __findNode(self, how_deep_should_we_dig_into):
        """
        A delegate method of updateSymbol to find the relevant node to update
        :param int - how_deep_should_we_dig_into
        :return: int - Index of the relevant node in the node_list
        """
        if how_deep_should_we_dig_into ==0:
            return 0 #index of the root is 0

        #reverse the history in 2 steps
        history_reverse = self.history[-how_deep_should_we_dig_into:]
        history_reverse.reverse() #reverse method returns nothing but operates on the object itself
        index=0
        for h in history_reverse:
            assert h=="0" or h=="1"
            if h=="0":
                index=(index+1)*2-1 #index of the left child
            else:
                index=(index+1)*2 #index of the right child
        return index



    def updateSymbols(self, symb_list):
        """
        update the context tree with a list of symbols.
        Assuming symb_list is for percept only
        :param symb_list: list of binary symbols
        :return: void
        """
        #Fill the first few symbols to the history without doing stuff


        for sym in symb_list:
            if len(self.history)< self.depth: #not <=
                self.history.append(sym)
                continue
            self.updateSymbol(sym)


    def updateLogProbability(self, relevant_node_index):
        """
        Calculate the logarithm of the weighted block probability.
        :return:
        """
        conditional_probability = 0.5
        if int(self.node_list[relevant_node_index].child_one)>=2**(self.depth+1)-1-1 and \
                        int(self.node_list[relevant_node_index].child_zero)>=2**(self.depth+1)-1-1: #leaf node condition
            self.node_list[relevant_node_index].log_prob_weighted = \
                self.node_list[relevant_node_index].log_prob_estimated
        else:
            current_depth = self.node_list[relevant_node_index].depth
            child_zero_index = self.node_list[relevant_node_index].child_zero
            child_one_index = self.node_list[relevant_node_index].child_one

            if self.depth>threshhold:
                child_zero_index=str(child_zero_index)
                child_one_index=str(child_one_index)

            #dynamically create nodes
            if self.shouldCreateNewNode(child_zero_index):
                self.node_list[child_zero_index]=CTNode(current_depth+1, child_zero_index)
            if self.shouldCreateNewNode(child_one_index):
                self.node_list[child_one_index]=CTNode(current_depth+1, child_one_index)




            updated_probability = 0.5*math.pow(2.0, self.node_list[relevant_node_index].log_prob_estimated) +\
                0.5* math.pow(2.0, self.node_list[child_zero_index].log_prob_weighted)* \
                math.pow(2.0, self.node_list[child_one_index].log_prob_weighted)


            try:
                assert 0<=updated_probability<=1
            except AssertionError:
                updated_probability=1


            try:
                self.node_list[relevant_node_index].log_prob_weighted = math.log2(updated_probability)
            except ValueError:
                self.resetProbability()

        return

    def updateHistory(self, symb_list):
        """
        updates the history statistics, without touching the context tree
        :param symb_list:
        :return: void
        """
        for symb in symb_list:
            self.history.append(symb)
        return

    def revert(self):
        """
        Removes the most recently observed symbol (percept symbols only) from the context tree, called by revertHistory
        :return: void
        """

        #if the length of the history is less than the depth of the tree, just remove the symbol.
        #Because those initial symbols were not added to the context tree in the first place.
        if len(self.history)<= self.depth:
            del self.history[-1]
            return

        last_observed_sym = self.history[-1]
        del self.history[-1]
         #look back the past "self.depth" symbols
        depth_counter = self.depth #minus 1 in every iteration
        while depth_counter>=0: #update the node from the deep end to the root
            #locate the node
            relevant_node_index = self.__findNode(depth_counter)
            if self.depth>threshhold:
                relevant_node_index=str(relevant_node_index)

            """

            Formula 23 and 24 in Vanness paper
            :param sym:
            :return: void
            """
            if last_observed_sym=="0":
                self.node_list[relevant_node_index].count_zero -= 1

            elif last_observed_sym=="1":
                self.node_list[relevant_node_index].count_one -= 1
            self.node_list[relevant_node_index].log_prob_estimated -= self.node_list[relevant_node_index].logKTMul(last_observed_sym)
            #reverse discount value applied
            self.node_list[relevant_node_index].log_prob_estimated = self.node_list[relevant_node_index].log_prob_estimated/discount_value
            self.updateLogProbability(relevant_node_index)
            depth_counter -=1
        return

    def revertHistory(self, new_size, no_action_bits, no_obs_bits, no_reward_bits, last_update_percept ):
        """
        Shrinks the history down to a former size, including deleting the records in the model for the simulated
        percepts and actions
        :param new_size: int - new history size
        :param no_action_bits: int - number of bits used to represent action
        :param no_obs_bits: int - number of bits used to represent observation
        :param no_reward_bits: int - number of bits used to represent reward
        :param last_update_percept:
        :return:

        """
        assert new_size <= len(self.history)

        while len(self.history) > new_size:

            #keep reducing the length until the history
            if len(self.history)<=self.depth:
                del self.history[-1]
                continue


            if last_update_percept == True: #revert (observation, reward) bits
                for b in range(0,no_reward_bits):
                    self.revert()

                for b in range(0, no_obs_bits):
                    self.revert()

                last_update_percept = False
            else: #revert action bits
                for b in range(0, no_action_bits):
                    del self.history[-1]
                last_update_percept = True

        return

    def genRandomSymbols(self, symbols, bits):
        """
        generate a specified number of random symbols
         distributed according to the context tree statistics
        :param symbols: list of symbols
        :param bits: number of bits
        :return: void
        """
        self.genRandomSymbolsAndUpdate(symbols, bits)

        # restore the context tree to it's original state
        for i in range(0, bits):
            self.revert()
        return

    def genRandomSymbolsAndUpdate(self, symbols, bits): #TODO
        """
        generate a specified number of random symbols
        distributed according to the context tree statistics
        and update the context tree with the newly
        generated bits
        :param symbols: list of symbols
        :param bits: int number of bits
        :return: String
        """
        assert bits>=1
        sym =""
        sym_list = ""
        length_before = len(self.history)

        for i in range(0, bits):
            sym = self.mostLikelyCharacter()
            self.updateSymbol(sym)
            sym_list += sym
        for i in range(0, bits):
            self.revert()
        assert sym_list

        assert len(self.history) == length_before + bits
        return sym_list

    def mostLikelyCharacter(self):
        """
        Log block probability of the whole sequence
        :return: double
        """

        root_index = 0
        if self.depth>threshhold:
            root_index = str(0)

        assert isinstance(self.node_list[root_index].log_prob_weighted, float)
        self.updateSymbol("0")
        block_probability_zero = self.node_list[root_index].log_prob_weighted
        self.revert()

        self.updateSymbol("1")
        block_probability_one = self.node_list[root_index].log_prob_weighted
        self.revert()

        if block_probability_zero>block_probability_one:
            self.updateSymbol("0")
            return "0"
        elif block_probability_zero<block_probability_one:
            self.updateSymbol("1")
            return "1"

        else: #when the probability break even
            sym = str(getrandbits(1))
            print(self.node_list[root_index].log_prob_weighted)
            print(self.node_list[root_index].log_prob_estimated)
            self.updateSymbol(sym)
            return sym


    def nthHistorySymbol(self, n):
        """
        get the n'th history symbol, None if doesn't exist
        :param n: int
        :return:
        """
        if n < len(self.history):
            return self.history[n]
        else:
            return None

    def numberOfNodes(self):
        """

        :return: int - number of nodes of the whole tree
        """
        if self.root:
            return self.root.size()
        else:
            return 0


