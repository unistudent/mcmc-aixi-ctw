import random

from environment import Environment


class TicTacToe(Environment):

    """
    https://en.wikipedia.org/wiki/Tic-tac-toe
    0|1|2
    ------
    3|4|5
    ------
    6|7|8

    """

    def __init__(self, options):
        super(Environment, self).__init__()
        self.m_observation = int("0"*9, 3) # 3 times 3 board
        #m_observation is a decimal integer, but the board is represented by base 3 numbers,
        #0 represent empty space, 1 represent agent, 2 represent the rival (the random computer)
        self.m_reward = 0
        self.game_over = False
        assert baseN(int("0"*9, 3), 3).zfill(9)=="0"*9
        self.total_games_played=1


    def performAction(self, action):
        """
        // receives the agent's action and calculates the new environment percept
        :param action: int: of a specific action type
        :return:
        """
        obs_symbs = self.extractObservationSymbols()
        #made an illegal move
        if obs_symbs[action]=="2" or obs_symbs[action]=="1":
            self.m_reward = -3
            self.game_over = True

            return

        else:
            #legal move
            obs_symbs[action]="1"
            obs_string = "".join(obs_symbs)
            self.m_observation= int(obs_string, 3)
            #after agent's turn, and agent find herself winning
            if self.__checkWinning("1"):
                self.m_reward = 2
                self.game_over=True
            elif self.__gridIsFull() and not self.__checkWinning("1") and not self.__checkWinning("2"):
                self.game_over=True
                self.m_reward = 1
            #Agent doesn't find herself winning or tie, time for opponent to respond
            else:
                self.__rivalPlays(obs_symbs)
                #In this case opponent wins
                if self.__checkWinning("2"):
                    self.m_reward=-2
                    self.game_over=True




    def isFinished(self):
        """

        :return: true if the environment cannot interact with the agent anymore
        """
        if self.game_over == True:
            return True

        for sym in baseN(self.m_observation, 3):
            if sym=="0":
                return False

        return True

    def __checkWinning(self, player_symb):

        """

        :param player_symb: String.  "1" for agent, "2" for rival
        :return: Boolean
        """
        obs_symbs = self.extractObservationSymbols()
        #Formed a row, #formed a column #Diagnal
        if obs_symbs[0]==obs_symbs[1]==obs_symbs[2]==player_symb \
            or obs_symbs[3]==obs_symbs[4]==obs_symbs[5]==player_symb \
            or obs_symbs[6] == obs_symbs[7] == obs_symbs[8] == player_symb \
            or obs_symbs[0]==obs_symbs[3]==obs_symbs[6]==player_symb \
            or obs_symbs[1]==obs_symbs[4]==obs_symbs[7]==player_symb \
            or obs_symbs[2]==obs_symbs[5]==obs_symbs[8]==player_symb \
            or obs_symbs[0]==obs_symbs[4]==obs_symbs[8]==player_symb \
            or obs_symbs[2]==obs_symbs[4]==obs_symbs[6]==player_symb:
            return True
        else:
            return False

    def extractObservationSymbols(self):
        """
        Convert decimal observation number into a base 3 string sequence
        :return:
        """
        return list(baseN(self.m_observation, 3).zfill(9)) #make sure the observation sequence is always 9 digit long

    def __rivalPlays(self, obs_symbs):
        obs_symbs = self.extractObservationSymbols()
        indexes=[]
        for x in range(0,9):
            if obs_symbs[x]=="0":
                indexes.append(x)
        obs_symbs[random.choice(indexes)]="2"
        self.m_observation = int("".join(obs_symbs), 3)

    def __gridIsFull(self):
        """

        :return: Boolean, returns true if the grid is full
        """
        obs_symbs = self.extractObservationSymbols()
        for sym in obs_symbs:
            if sym=="0":
                return False

        return True


def baseN(num,b,numerals="0123456789abcdefghijklmnopqrstuvwxyz"):
    """
    A helper function that converts an integer into a string of any length
    :param num:
    :param b:
    :param numerals:
    :return:
    """
    return ((num == 0) and numerals[0]) or (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
