import configparser
from random import random
import agent
from coinflip import CoinFlip
from search import search
from tictactoe import TicTacToe


def main_loop(ai, options, env, log):
    """
    The main agent/environment interaction loop
    :param env: Environment
    :param ai: Agent
    :type options: dictionary
    :return:
    """
    #Set up log file table head
    log.write("episode, cycle, observation, reward, action, explored, explore_rate, total_reward, average_reward\n")

    exploration = (float(options["exploration"])>0)
    explore_rate = float(options["exploration"])
    num_sim = int(options["mc-simulations"])
    if exploration == True:

        explore_decay = float(options["explore-decay"])
        assert 0<= explore_rate <=1.0
        assert 0<=explore_decay<=1.0

    if "termination-lifetime" in options.keys() and float(options["termination-lifetime"])>0:

        terminate_check = True
    else:
        terminate_check = False


    if terminate_check:
        terminate_lifetime = options["termination-lifetime"]
        assert 0<= terminate_lifetime

    cycle = 1
    episode = 1
    while True:
        #loop over episodes
        if env.isFinished():
            # Get a percept from the environment
            reward = env.getReward()
            ai.m_total_reward += reward


        env.__init__(options)
        while not env.isFinished():

            # check for agent termination
            if terminate_check and ai.m_time_cycle > terminate_lifetime:
                log.write("The agent has reached the end of lifetime")
                break
            # Get a percept from the environment
            observation = env.getObservation()
            reward = env.getReward()

            #Update agent's environment model with the new percept
            ai.modelUpdateX(observation, reward)

            # Determine best exploitive action, or explore
            explored = False
            if exploration and random()<explore_rate:
                action = ai.genRandomAction()
                explored = True
            else:
                action = search(ai, num_sim)
            #Send the action to the environment
            env.performAction(action)
            #Update the agent's environment model
            print ("Adding action: ", action, " to the model.")
            ai.modelUpdateA(action)

            #log into file
            csv_row = [str(episode), str(cycle), str(observation), str(reward), str(action), "yes" if explored else "no", \
                      str(explore_rate), str(ai.m_total_reward), str(ai.averageReward())]
            log.write(",".join(csv_row)+"\n")
            #print to console if the cycle is a power of 2
            if cycle & (cycle-1)==0:
                print ("episode", episode,"cycle: ", cycle)
                print ("average reward: ", ai.averageReward())
                if exploration:
                    print ("explore rate: ", explore_rate)


            #explore rate decays slowly
            if exploration:
                explore_rate *= explore_decay


            #at the end print summary to the standard output
            print ("Summary: ", "episode", episode, "agent lifetime: ", ai.m_time_cycle)
            print ("average reward: ", ai.averageReward())


            cycle += 1

        episode+=1

    return 0


def read_conf(conf_file_path):
    """
    Store the key value pairs
    :param conf_file_path:
    :return: the key value pairs of the config file in the format of a dictionary
    """

    cp = configparser.RawConfigParser()
    cp.read(conf_file_path)
    options = cp.items("learning-environment")  # options is a list of tuples
    options = dict(options)  # options is converted into a dictionary
    return options


def main(conf_file_path, log_file_path):
    """

    :param conf_file_path: String of full filepath of the config file
    :param log_file_path:  String of full filepath of the log file path
    :return:
    """
    while not conf_file_path:
        conf_file_path = input("Enter a valid full path of the config file. For example: coinflip.conf \n")


    while not log_file_path:
        log_file_path = input("Enter a valid full path of the output log file. For example: samplerun.csv \n")


    """
    Set up configuration options
    """
    options = read_conf(conf_file_path)
    if "ct-depth" not in options.keys():
        options["ct-depth"] = "3"
    if "agent-horizon" not in options.keys():
        #options["agent-horizon"] = "16"
        options["agent-horizon"] = "5"
    if "exploration" not in options.keys():
        options["exploration"] = "0"  # do not explore
    if "explore-decay" not in options.keys():
        options["explore-decay"] = "1.0"  # exploration rate does not decay


    """
    Read environment
    """
    environment_name = options["environment"]
    print (environment_name)
    if environment_name == "coin-flip":
        env = CoinFlip(options)
        options["agent-actions"] = "2"
        options["observation-bits"] = "1"
        options["reward-bits"] = "1"

    elif environment_name == "tic-tac-toe":
        env = TicTacToe(options)
        options["agent-actions"] = "9"
        options["observation-bits"] = "18"
        options["reward-bits"] = "3"

    else:
        print ("unknown environment, ", environment_name)
        return -1
    print(options)
    """
    Set up the agent
    """
    ai = agent.Agent(options)


    """
    Set up logging enviornment
    """
    log = open(log_file_path,'w')
    """
	Run the main agent/environment interaction loop
	"""
    main_loop(ai,  options, env, log)

    """
    Close the log file
    """
    log.close()




if __name__ == '__main__': main("", "")
