import abc

class Environment(object):
    """
    This is an abstract class:
    Constructor of the class sets up the initial environment percept
    https://pymotw.com/2/abc/
    """
    __metaclass__ = abc.ABCMeta #define it as an abstract class


    def performAction(self, action):
        """
        // receives the agent's action and calculates the new environment percept
        :param action: of a specific action type
        :return:
        """


    def isFinished(self):
        """

        :return: true if the environment cannot interact with the agent anymore
        """


    def getObservation(self):
        return self.m_observation #current observation

    def getReward(self):
        return self.m_reward #current reward

