import sys
# search options
from random import random, sample

import time

import math

import operator
from numpy import ndarray

from agent import ModelUndo

MinVisitsBeforeExpansion = 1
MaxDistanceFromRoot = 100
# MaxSearchNodes = sys.maxint

# Exploration/exploitation constant C
C = 0.01


# contains information about a single state
# in planning (search), we don't update the CTW

class SearchNode(object):
    def __init__(self, h, chance_node_or_not):
        """

        :param h:
        :param chance_node_or_not: Boolean
        :return:
        """
        self.h = h #not the whole history, just the history related to simulation
        self.is_chance_node = chance_node_or_not  # true if this node is a chance node, false otherwise
        self.expected_reward = 0.0  # the expected reward of this node
        self.no_visits = 0  # number of times the search node has been visited
        self.child_nodes = list()

    def selectAction(self, agent, dfr):  # called by sample
        """
        determine the next action to play

        :param agent: Agent
        :return: int
        """
        untaken_actions = set()
        #Untaken actions = uncreated nodes + created childnodes with no_visit = 0
        #uncreated nodes
        for a in range(0, agent.m_actions):  # creating nodes on the fly
            ha = self.h + agent.encodeAction(a)
            if not self.__childNodeHasBeenCreated(ha):
                untaken_actions.add(a)

        #child_node with 0 visit
        for child_node in self.child_nodes:
            if child_node.no_visits == 0:
                untaken_actions.add(agent.decodeAction(child_node.h[-agent.m_actions_bits:]))


        # random.sample returns a list, thus index 0 is necessary, create a child_node with it
        if untaken_actions != set():
            some_untaken_action = sample(untaken_actions, 1)[0]

            ha = self.h + agent.encodeAction(some_untaken_action)
            if not self.__childNodeHasBeenCreated(ha):  # create node if the child node has never been created
                self.child_nodes.append(SearchNode(ha, True))
            assert len(self.child_nodes)<= agent.m_actions

            #print "Action: ", some_untaken_action
            return some_untaken_action
        else:  # if all action has been sampled before

            # Return the a that causes the highest expected value, skewed towards less taken actions
            candidate_dict = {}
            for child_node in self.child_nodes:
                candidate_dict[child_node.h] = 1 / dfr * child_node.expected_reward/(agent.maxReward()-agent.minReward()) + \
                                               C * math.sqrt(math.log2(self.no_visits) / child_node.no_visits)

            assert len(candidate_dict) <= agent.m_actions
            #Note, because reward is already normalised in each calculation, we took out the normalisation factor
            #(beta - alpha) as on P11 of the paper
            max_key = max(candidate_dict.items(), key=operator.itemgetter(1))[0]

            encoded_action = max_key[-agent.m_actions_bits:]
            #print "Action: ", encoded_action
            return agent.decodeAction(encoded_action)

    def __childNodeHasBeenCreated(self, hypothetical_h):
        """
        Check whether a node representing history h has been created among the node's child_nodes
        :param h:
        :return: boolean
        """
        has_been_created = False
        for child_node in self.child_nodes:
            if child_node.h == hypothetical_h:
                has_been_created = True

        return has_been_created

    def __getChildNode(self, h_to_find):
        """
        Find the child node with the corresponding history
        :param h_to_find: string
        :return: SearchNode
        """
        for child_node in self.child_nodes:
            assert type(child_node) is SearchNode
            if child_node.h == h_to_find:
                return child_node







    def sample(self, agent, dfr):  # called by search
        """
        A recursive function perform a (ONE AND ONLY ONE) sample run through this node and it's children,
	    returning the accumulated reward from this sample run
        :param agent: Agent
        :param dfr: int - depth forward remaining
        :return: float
        """

        reward = 0.0
        if dfr == 0:
            return 0.0
        elif self.is_chance_node:

            o, r = agent.genPerceptAndUpdate()
            assert o == agent.decodeObservation(agent.encodeObservation(o))
            assert r == agent.decodeReward(agent.encodeReward(r))

            hor = self.h + agent.encodePercept(o, r)
            if not self.__childNodeHasBeenCreated(hor):  # create node if the child node has never been created
                self.child_nodes.append(SearchNode(hor, False))

            #child nodes of a chance node cannot be more than the possibile combo of observation and reward
            assert len(self.child_nodes)<= math.pow(2, agent.m_obs_bits) * math.pow(2, agent.m_rew_bits)

            the_decision_node = self.__getChildNode(hor)

            reward = r + the_decision_node.sample(agent, dfr - 1)

        elif self.no_visits == 0:  # the decision node has been visited before, and it's not a chance node
            reward = playout(agent, dfr)

        else: #if it's a deicions node, and it has been visited before

            action = self.selectAction(agent, dfr)
            agent.modelUpdateA(action)#update action in the model
            ha = self.h + agent.encodeAction(action)

            if not self.__childNodeHasBeenCreated(ha):  # create node if the child node has never been created
                self.child_nodes.append(SearchNode(ha, True))
            assert len(self.child_nodes)<= agent.m_actions
            the_chance_node = self.__getChildNode(ha)

            reward = the_chance_node.sample(agent, dfr)

        self.expected_reward = 1 / (self.no_visits + 1) * (reward + self.no_visits * self.expected_reward)
        self.no_visits += 1  # Increment the number of visits

        return reward

def playout(agent, playout_len):  # TODO aka rollout
    """
    simulate a path through a hypothetical future for the agent within it's
    internal model of the world, returning the accumulated reward.
    :param agent: Agent
    :param playout_len: int
    :return: float
    """
    reward = 0.0
    #print "Roll out"
    for t in range(0, playout_len):

        action = agent.genRandomAction() #rollout action selection strategy
        agent.modelUpdateA(action) #Update simulated action

        o, r = agent.genPerceptAndUpdate() #Update simulated observation and reward

        reward += r

    return reward


def search(agent, num_sim):  # TODO
    """
    Determine the best action by searching ahead using MCTS
    :param agent: agent
    :param num_sim: int - number of times the agent samples in each cycle
    :return: int
    """
    search_tree = SearchNode("", False)  # first node is to take an action

    for x in range(0, num_sim):
        #The following code in the loop complete one sample act.
        #With each sample act, the reversion needs to take place because it is not part of the actual history
        restore_point = ModelUndo(agent)
        search_tree.sample(agent, agent.m_horizon)
        agent.modelRevert(restore_point)


    return search_tree.selectAction(agent, agent.m_horizon)

