import random

import sys

from predict import ContextTree


class Agent(object):
    def __init__(self, options):
        """
        Construct a learning agent from the command line arguments
        """

        self.m_actions = int(options["agent-actions"])  # m_actions is in int.
        self.m_horizon = int(options["agent-horizon"])  # length of the search horizon used by the agent
        self.m_obs_bits = int(options["observation-bits"])
        self.m_rew_bits = int(options["reward-bits"])
        self.m_ct = ContextTree(int(options["ct-depth"]))  # hmmmm
        self.m_last_update_percept = False

        """
        calculate the number of bits needed to represent the action
        """
        self.m_actions_bits = (self.m_actions-1).bit_length()
        self.reset()  # set self.m_time_cycle to 0, and self.m_total_reward to 0.0

    def averageReward(self):
        if self.m_time_cycle > 0:
            return self.m_total_reward / self.m_time_cycle
        else:
            return 0.0

    def maxReward(self):
        """
        maximum reward in a single time instant
        :return:  float
        """
        return 1 << self.m_rew_bits - 1

    def minReward(self):
        """
        Minimum reward in a single time instant
        :return: float
        """
        return 0.0

    def historySize(self):
        """
        the length of the stored history for an agent

        :return: int
        """
        return self.m_ct.historySize()

    def genRandomAction(self):
        """
        generate an action uniformly at random
        :return: int
        """
        return random.randint(0, self.m_actions - 1)

    def genPercept(self):
        """
        #TODO
        generate a percept distributed according
         to our history statistics
        :return: obervation reward pair
        """
        self.genPerceptAndUpdate()

        # TODO: Revert

        return o, r

    def genPerceptAndUpdate(self):
        """
        Generate a percept distributed to our history statistics, and update our mixture environment model with it
        This is for simulating the future only.
        :return: observation reward pair
        """
        assert self.m_last_update_percept == False, "Percept has just been updated. Cannot update percept again."
        #reward depends on observation, so observation and reward symbols should be generated and updated one by one
        o_symbs = self.m_ct.genRandomSymbolsAndUpdate(None, self.m_obs_bits)
        r_symbs = self.m_ct.genRandomSymbolsAndUpdate(None, self.m_rew_bits)


        o = self.decodeObservation(o_symbs)
        r = self.decodeReward(r_symbs)
        # Update other properties
        self.m_total_reward += r
        self.m_last_update_percept = True
        return o, r

    def modelUpdateX(self, observation, reward):
        """
        Update the agent's internal model of the world after receiving a percept
        :param observation:
        :param reward:
        :return:
        """
        # Update internal model
        assert self.m_last_update_percept == False, "Percept has just been updated. Cannot update percept again."
        percept = self.encodePercept(observation, reward)  # percept is a list of boolean symbols
        # Start update tree only when history is as long as the depth of the context tree

        for sym in percept:

            if len(self.m_ct.history) >= self.m_ct.depth:
                #print ("Adding percept ", sym, " to the model.")
                self.m_ct.updateSymbols(sym)
            # self.m_ct.updateHistory(percept) NO! UpdateHistory is for action only
            else:
                self.m_ct.history += list(sym) #when the length of the history is less than the depth of the tree
            #If I had used append, it would become a list as an element within history
        # Update other properties
        self.m_total_reward += reward
        self.m_last_update_percept = True
        return

    def modelUpdateA(self, action):
        """
        Update the agent's internal model of the world after performing an action

        :param action:
        :return:
        """
        assert self.isActionOK(action), "action is not valid"
        assert self.m_last_update_percept == True, "Percept has not been updated. Cannot perform action."
        action_symbs = self.encodeAction(action)
        # self.m_ct.updateSymbols(action_symbs) No No this is only for updatingPercept not for action
        self.m_ct.updateHistory(action_symbs)

        self.m_time_cycle += 1
        self.m_last_update_percept = False
        return

    def isActionOK(self, action):
        return False

    def modelRevert(self, model_undo):
        """

        :param model_undo: An instance of model_undo object
        :param self:
        :return:
        """
        self.m_total_reward = model_undo.m_total_reward
        self.m_ct.revertHistory(model_undo.m_history_size, self.m_actions_bits,
                                self.m_obs_bits, self.m_rew_bits, self.m_last_update_percept)
        # need to pass in action bits,  observation bits, reward bits, and whether percept has been updated last
        self.m_time_cycle = model_undo.m_time_cycle
        self.m_last_update_percept = model_undo.m_last_update_percept
        return

    def reset(self):
        """

        :rtype: object
        """
        self.m_ct.clear()
        self.m_time_cycle = 0
        self.m_total_reward = 0.0
        return

    def getPredictedActionProb(self, action):
        """
        probability of selecting an action according to the
        agent's internal model of it's own behaviour

        :param action:
        :return: double
        """
        return 0.0  # TODO

    def perceptProbability(self, observation, reward):
        """
        get the agent's probability of receiving a particular percept

        :param observation:
        :param reward:
        :return: double
        """

        return 0.0  # TODO

    def isActionOK(self, action):
        """
        Check if the action is within the range of allowed actions
        :param action: int
        :return: true if the action is valid, false otherwise
        """
        return action < self.m_actions

    def isRewardOK(self, reward):
        """
        Check if the reward is between the minimum and maximum reward value
        :param reward: int
        :return: true if the reward is within the range, false otherwise
        """
        return self.minReward() <= reward <= self.maxReward()

    def encodeAction(self, action):
        """
        Encode action into a list of symbols
        :param action:
        :return: string of boolean
        """
        return ('{0:0' + str(self.m_actions_bits) + 'b}').format(action)

    def encodePercept(self, observation, reward):
        """
        Encodes a percept (observation, reward) as a list of symbols
        :param observation: int
        :param reward: int
        :return: string of boolean
        """
        obs_string = self.encodeObservation(observation)
        rew_string = self.encodeReward(reward)
        assert reward == self.decodeReward(rew_string)
        assert observation== self.decodeObservation(obs_string)
        return obs_string+rew_string

    def encodeObservation(self, observation):
        return ('{0:0' + str(self.m_obs_bits) + 'b}').format(observation)

    def encodeReward(self, reward):
        if reward<0:
            return '1'+('{0:0' + str(self.m_rew_bits-1) + 'b}').format(int(abs(reward)))
        else:
            return ('{0:0' + str(self.m_rew_bits) + 'b}').format(int(reward))
    def decodeAction(self, symb_list):
        """
        Decodes the Action from string
        :param symb_list: string of boolean
        :return: int
        """
        return int(symb_list, 2)

    def decodeObservation(self, symb_list):
        """
        Decodes the observation from String
        :param symb_list: String of boolean
        :return: int
        """
        return int(symb_list, 2)

    def decodeReward(self, symb_list):
        """
        Decodes the reward from a list of symbols
        :param symb_list: String of boolean
        :return: float
        """
        if self.m_actions_bits==1:
            reward = int(symb_list, 2)

            return float(reward)
        elif self.m_actions_bits>1:
            sign = symb_list[0]
            symb_list = symb_list[1:]
            if sign=="1":
                return float(-1*int(symb_list, 2))
            else:
                return float(int(symb_list, 2))


class ModelUndo(object):
    """
    used to store sufficient information to revert an agent
    to a copy of itself from a previous time cycle
    """

    # Used to revert an agent to a previous state
    def __init__(self, agent):
        """
        Construct a save point
        :param agent: type Agent
        :return:
        """
        self.m_time_cycle = agent.m_time_cycle
        self.m_total_reward = agent.m_total_reward
        self.m_history_size = len(agent.m_ct.history)
        self.m_last_update_percept = agent.m_last_update_percept
