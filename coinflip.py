from random import random

import abc

from environment import Environment


class CoinFlip(Environment):
    def __init__(self, options):
        super(Environment, self).__init__()
        self.p = float(options["coin-flip-p"])

        assert 0<= self.p <= 1.0
        self.m_observation = 1 if random()<self.p else 0
        self.m_reward = 0


    def performAction(self, action):
        self.m_observation = 1 if random()<self.p else 0
        self.m_reward = 1 if self.m_observation == action else 0



    def isFinished(self):

        return False



